import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class MessageParser {
	private static final int MAX_EMOTICON_LENGTH = 15;
	public static final String URL_REGEX = "\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
	
	public static class Link{
		public final String url;
		public final String title;
		
		public Link(String url, String title){
			this.url = url;
			this.title = title;
		}
	}
	

	public static void main (String[] args){
		if (args.length != 1){
			System.out.println("Run the program with a message as an argument, in quotes");
			System.out.println("For example \"This is a message with a @mention, an (emoticon) and http://www.a.website.com/\"");
			return;
		}
		String message = args[0];
		if (message == null || message.isEmpty()){
			System.out.println("Message was empty");
			return;
		}
		
		System.out.println(parseMessage(message));
	}
	
	public static String parseMessage(String message){
		List<String> mentions = getMentions(message);
		List<String> emoticons = getEmoticons(message);
		List<Link> links = getLinks(message);
		
		JSONObject mainObject = new JSONObject();

		if (!mentions.isEmpty()){
			mainObject.put("mentions", mentions);
		}
		
		if (!emoticons.isEmpty()){
			mainObject.put("emoticons", emoticons);
		}
		if (!links.isEmpty()){
			for (Link link : links){
				JSONObject linkObject = new JSONObject();
				linkObject.put("url", link.url);
				linkObject.put("title", link.title);
				mainObject.append("Links", linkObject);
			}
		}
		return mainObject.toString();
	}

	private static List<String> getMentions(String message) {
		List<String> mentions = new ArrayList<>();
		String[] splitWords = message.split("@");

		// We ignore the first entry, so if there are more than 1 entries we give up
		if (splitWords == null || splitWords.length <= 1){
			return mentions;
		}
		
		for (int i = 1; i < splitWords.length; i++){
			// Find the first word by finding the first non-word character
			String firstWord = splitWords[i].split("\\W")[0];
			if (firstWord.isEmpty()){
				continue;
			}
			mentions.add(firstWord);
		}
		
		return mentions;
	}

	private static List<String> getEmoticons(String message) {
		List<String> emoticons = new ArrayList<>();

		String[] splitWords = message.split("\\(");
		// We ignore the first entry, so if there are more than 1 entries we give up
		if (splitWords == null || splitWords.length <= 1){
			return emoticons;
		}
		
		for (int i = 1; i < splitWords.length; i++){
			// Find the emoticon by finding the first ')' character
			String emoticon = splitWords[i].split("\\)")[0];
			// If there is no closing bracket or the emoticon is longer than 15 characters, we skip it
			if (emoticon.length() == splitWords[i].length() || emoticon.length() > MAX_EMOTICON_LENGTH){
				continue;
			}
			emoticons.add(emoticon);
		}

		return emoticons;
	}

	private static List<Link> getLinks(String message) {
		List<Link> links = new ArrayList<>();

		// Just using a regex I found on Stack Overflow.
		Pattern pattern = Pattern.compile(URL_REGEX);
		Matcher matcher = pattern.matcher(message);
		while (matcher.find()){
			String url = (message.substring(matcher.start(0), matcher.end(0)));
			// Do a request to find title, using screen-scraping library Jsoup
			Document doc;
			try {
				doc = Jsoup.connect(url).get();

			} catch (IOException e) {
				// Url was invalid or whatever. Ignore.
				continue;
			}

			// We'll ignore it if we can't find the title
			Elements titles = doc.getElementsByTag("title");
			if (titles == null){
				continue;
			}
			Element title = titles.get(0);
			if (title == null){
				continue;
			}

			Link link = new Link(url, title.text());
			links.add(link);
		}
		
		return links;
	}
}
